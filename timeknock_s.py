#!/usr/bin/env python

import totp
import socket
import time
import easyufw.easyufw as ufw # uncomplicated firewall interface

def gatekeep(key):
    ufw.enable()
    ufw.allow('34999/udp')
    ufw.deny('22/tcp')
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.bind(('0.0.0.0',34999))

    while True:
        r,addr = s.recvfrom(32)
        if r == totp.get_full_totp(key):
            ufw.allow('22/tcp') # unlocked
            time.sleep(5)
            ufw.deny('22/tcp') # locked
        time.sleep(5)

if __name__ == '__main__':
    key = '<SUPER_SECRET_KEY_HERE>'
    gatekeep(key)
