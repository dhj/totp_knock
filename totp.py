import time
import hmac
import hashlib
import struct
import base64
import os

def get_full_totp(key,timestamp=None,interval_length=30, digest_method=None):

	if timestamp is None:
		timestamp = time.time()

	if digest_method is None:
		digest_method = hashlib.sha256

	time_count = int(timestamp) // interval_length
	msg = struct.pack('>Q', time_count)
	hmac_digest = hmac.new(key,msg,digest_method).digest()

	return hmac_digest

def generate_secret():
    r = os.urandom(32)
    return base64.b32encode(r)

if __name__ == '__main__':
    print generate_secret()
