
Use TOTP and a shared key for [port knocking][archknock]
to a single UDP port.

The server and client share a key.  The
client creates a hash based on the key
and time (same as [totp][]).  The full hash
is sent to a UDP port.  If the server
confirms a matching hash it opens ssh
port.

This uses [easyufw][] to handle dynamic firewall
configuration.

On the server:

```
# dependency
sudo apt-get install python-ufw

# get totp_knock
git clone https://gitlab.com/dhj/totp_knock
cd totp_knock

sudo python timeknock_s.py  # run service
```

On the client:

```
# get totp_knock
git clone https://gitlab.com/dhj/totp_knock
cd totp_knock

# connect
ssh <host>  # won't work, server is blocking
python timeknock_c.py <host> && ssh <host>  # should work 
```


## Benefits
(similar to port knocking)

1. Even if the port and technique are
identified an attacker should not be
able to access port 22 (in sample code
they could still piggyback when the port
is opened, but that can be fixed).

2. Reduces logs of constant bot attacks on
port 22.

3. Reduces the ability of an attacker to
fingerprint a networked computer (udp
port does nothing unless it receives the
correct packet)

## Pitfalls

1. This is a proof of concept only, do not
use this in production! It is not robust
to errors.

2. When the sample code is run it will
   activate the ufw firewall.  By
default ufw blocks all incoming ports.

3. When port 22 of the firewall is
   opened it is opened for all ip
addresses rather than the address that
sent the packet.

4.  The sample code is non-concurrent.
    It opens the port and sleeps for a
few seconds. If the previous pitfall is fixed (open to sender only)
and two clients try
to initiate a connection at the same time then the server
would need to handle connections concurrently.

## Potential Improvements

1. Make the server code robust against
errors and run as a daemon (restarts,
logs, etc).

2. Use environment variables for keys

3. Open the port only for the address that
sent the packet.

4. Adjust open time and key change time to
be equal (5 seconds each)?

5. Make a particular key only work once to open
the port and only for the address
that sent the packet.  This would
prevent a replay attack.  It would not
increase vulnerability to denial of
service attack (success for the time
period still requires correct 256 bits).

6. Combining 3 and 4 with the existing
   non-concurrent sleep behavior would have the same
effect as 5 (only once per key).
Although concurrent connection starts
would still be a problem.
Implementation of one port opening per valid key would
inherently limit throughput.

## Similar Tech

Integration of TOTP with knocking has
been done by making the knock ports
change based on a shared key and TOTP
hashing by [sshflux][].

Remote authorization of firewall rule
changes has been implemented using
encrypted messages to a server with a
technique called [Single Packet
Authorization][singlepacket1]
([linuxjournal article][singlepacket2]).

The [wiki page on port
knocking][wikiknock] also has several
thesis references to port knocking.

I did not do a thorough literature
search.  There are probably other implementations
that are even more similar to TOTP Knock
than these.  Please let me know about them!

## Disclaimer

WARNING:  **DO NOT USE THIS IN
PRODUCTION**.  The code is a proof of
concept.  If you use this, make
sure you have console access to the
machine.  If you find a bug you can lose
ssh access and will need a console to
reset the firewall.

The following command on the server will reset the
firewall:

`sudo ufw disable`

The test code will lock down all
incoming ports
(default for ufw)
and listen on UDP port 34999.  It
will only open port 22 (ssh) when it
receives a correct signal based on the
time and shared key. A key is valid for
30 seconds and sending a valid key will
open the port for 5 seconds.  Outgoing
ports are not restricted by default with
ufw.

Do not use this as your only ssh
security.  By design the software will
open the ssh port for a few seconds at a
time.  This only reduces the attack
profile of a computer connected to the
internet (similar to port knocking).

## License

The sample code is released under an MIT
license.  Enjoy!

[sshflux]:https://blog.benjojo.co.uk/post/ssh-port-fluxing-with-totp
[easyufw]:https://gitlab.com/dhj/easyufw
[archknock]:https://wiki.archlinux.org/index.php/Port_knocking
[totp]:https://tools.ietf.org/html/rfc6238
[singlepacket1]:http://www.cipherdyne.org/fwknop/docs/SPA.html
[singlepacket2]:http://www.linuxjournal.com/article/9621?page=0,1
[wikiknock]:https://en.wikipedia.org/wiki/Port_knocking
