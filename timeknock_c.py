#!/usr/bin/env python

import totp
import socket
import sys

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
default_port = 34999

def main(d_addr):
    key = '<SUPER_SECRET_KEY_HERE>'
    code = totp.get_full_totp(key)
    print "sending:",code.encode('hex')
    sock.sendto(code,d_addr)

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print "Usage:"
        print sys.argv[0], "host[:port]"
        print
        print "where host is ip or resolvable hostname and port default is 34999"
        sys.exit()

    d_addr = sys.argv[-1].split(':')
    if len(d_addr) < 2:
        d_addr.append(default_port)  # no port, add default
    else:
        d_addr[-1] = int(d_addr[-1]) # with port, convert to int

    main(tuple(d_addr))
